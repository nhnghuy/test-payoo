using System;

namespace test_payoo
{
    public interface IMessage
    {
        DateTime CreatedAt { get; set; }
    }
}