namespace test_payoo
{
    public interface IRequest : IMessage
    {
        string RequestId { get; set; }

        /// <summary>
        /// ID of employee that make request
        /// </summary>
        string RequesterId { get; set; }

        /// <summary>
        /// Inside code of the region
        /// </summary>
        string RegionCode { get; set; }

        /// <summary>
        /// Inside code of the shop that make request
        /// </summary>
        string WarehouseCode { get; set; }
        
        /// <summary>
        /// IP address of the shop that make request
        /// </summary>
        string IPAddress { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        string Operation { get; }
    }

    public interface ICustomerRequest : IRequest
    {
    }
}