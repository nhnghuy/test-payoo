﻿using System.Xml.Serialization;
using System;
using System.Collections.Generic;

namespace test_payoo
{
    #region Request
    public abstract partial class PayooRequestData : IRequest
    {
        [XmlRoot("QueryBillExRequest")]
        public partial class GetBill : PayooRequestData, ICustomerRequest
        {
            public override string Operation { get => "MMS_QueryBillEx"; }
            public override Type GetResponseType() => typeof(PayooResponseData.GetBill);

            [XmlElement(ElementName = "CustomerId")]
            public string CustomerId { get; set; }

            [XmlElement(ElementName = "ServiceId")]
            public string ServiceId { get; set; }

            [XmlElement(ElementName = "ProviderId")]
            public string ProviderId { get; set; }

            [XmlElement(ElementName = "Capcha")]
            public string Captcha { get; set; }

            [XmlElement(ElementName = "Area")]
            public string Area { get; set; }

            [XmlElement(ElementName = "HouseNumber")]
            public string HouseNumber { get; set; }
        }
    }
    #endregion

    #region Response
    public abstract partial class PayooResponseData : IResponse
    {
        [XmlRoot("QueryBillExResult")]
        public class GetBill : PayooResponseData
        {
            [XmlElement(ElementName = "Capcha")]
            public byte[] Captcha { get; set; }

            // [XmlElement(ElementName = "Bills")]
            [XmlArray("Bills")]
            [XmlArrayItem("BillInfo")]
            public List<Bill> Bills { get; set; }

            // [XmlElement(ElementName = "Services")]
            [XmlArray("Services")]
            [XmlArrayItem("Service")]
            public List<Serivce> Services { get; set; }

            [XmlElement(ElementName = "VietUnionId")]
            public string VietUnionId { get; set; }

            // [XmlElement(ElementName = "CustomerInfos")]
            [XmlArray("CustomerInfos")]
            [XmlArrayItem("CustomerInfo")]
            public List<Customer> Customers { get; set; }

            [XmlElement(ElementName = "MatchServiceCount")]
            public int? MatchServiceCount { get; set; }

            [XmlElement(ElementName = "PaymentRule")]
            public int? PaymentRule { get; set; }

            [XmlElement(ElementName = "DescriptionCode")]
            public string DescriptionCode { get; set; }

            [XmlElement(ElementName = "PaymentFeeType")]
            public int? PaymentFeeType { get; set; }

            [XmlElement(ElementName = "PercentFee")]
            public decimal? PercentFee { get; set; }

            [XmlElement(ElementName = "ConstantFee")]
            public decimal? ConstantFee { get; set; }

            [XmlElement(ElementName = "MinFee")]
            public decimal? MinFee { get; set; }

            [XmlElement(ElementName = "MaxFee")]
            public decimal? MaxFee { get; set; }
        }
    }
    #endregion

    public class Bill
    {
        [XmlElement(ElementName = "BillId")]
        public string BillId { get; set; }

        [XmlElement(ElementName = "ServiceId")]
        public string ServiceId { get; set; }

        [XmlElement(ElementName = "ProviderId")]
        public string ProviderId { get; set; }

        [XmlElement(ElementName = "Month")]
        public string Month { get; set; }

        [XmlElement(ElementName = "MoneyAmount")]
        public decimal? MoneyAmount { get; set; }

        [XmlElement(ElementName = "PaymentFee")]
        public decimal? PaymentFee { get; set; }

        [XmlElement(ElementName = "CustomerName")]
        public string CustomerName { get; set; }

        [XmlElement(ElementName = "Address")]
        public string Address { get; set; }

        [XmlElement(ElementName = "ExpiredDate")]
        public string ExpiredDate { get; set; }

        [XmlElement(ElementName = "IsPrepaid")]
        public bool IsPrepaid { get; set; }

        [XmlElement(ElementName = "MonthAmount")]
        public int? MonthAmount { get; set; }

        [XmlElement(ElementName = "PaymentRange")]
        public string PaymentRange { get; set; }

        [XmlElement(ElementName = "RenewalDate")]
        public string RenewalDate { get; set; }

        [XmlElement(ElementName = "BusinessOrderNo")]
        public string BusinessOrderNo { get; set; }

        [XmlElement(ElementName = "BusinessUrl")]
        public string BusinessUrl { get; set; }

        [XmlElement(ElementName = "BusinessName")]
        public string BusinessName { get; set; }

        [XmlElement(ElementName = "ShippingDateNum")]
        public string ShippingDateNum { get; set; }

        [XmlElement(ElementName = "FromShippingDay")]
        public string FromShippingDay { get; set; }

        [XmlElement(ElementName = "EcommerceDesc")]
        public string EcommerceDesc { get; set; }

        [XmlElement(ElementName = "InfoEx")]
        public string InfoEx { get; set; }
    }

    public class Serivce
    {
        [XmlElement(ElementName = "ServiceId")]
        public string Id { get; set; }

        [XmlElement(ElementName = "ServiceName")]
        public string Name { get; set; }

        // [XmlElement(ElementName = "Issuers")]
        [XmlArray("Issuers")]
        [XmlArrayItem("Issuer")]
        public List<Issuer> Issuers { get; set; }

        [XmlElement(ElementName = "MatchProviderCount")]
        public int? IssuersCount { get; set; }
    }

    public class Issuer
    {
        [XmlElement(ElementName = "IssuerId")]
        public string Id { get; set; }

        [XmlElement(ElementName = "IssuerName")]
        public string Name { get; set; }

        [XmlElement(ElementName = "IsOnline")]
        public bool IsOnline { get; set; }
    }

    public class Customer
    {
        [XmlElement(ElementName = "CustomerId")]
        public string Id { get; set; }

        [XmlElement(ElementName = "Title")]
        public string Title { get; set; }

        [XmlElement(ElementName = "IdentityCode")]
        public string IdentityCode { get; set; }

        [XmlElement(ElementName = "MoneyAmount")]
        public string Amount { get; set; }
    }

}
