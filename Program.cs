﻿using System;
using System.Text;
using System.Net.Http;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace test_payoo
{
    class Program
    {
        public const string ENDPOINT_URI = "https://vusandbox.payoo.com.vn/UniGWS.asmx";
        public const string CLIENT_PASSWORD = "UVyzA0FUogvY/L4opFOO8M57jBByU804Nylhd8EKJeU=";
        public const string PKCS12_FILEPATH = "82_Sandbox.p12";
        public const string PKCS12_PASSWORD = "123456789";

        static void Main(string[] args)
        {
            try
            {
                TestRequest();
                // TestFakeResponseDeserialization();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private static void TestRequest()
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("SOAPAction", "UniGWS/Execute2");

            var httpContent = new StringContent(GenerateRequestBodyString(), Encoding.UTF8, "text/xml");

            var httpResponseMessage = httpClient.PostAsync(ENDPOINT_URI, httpContent).GetAwaiter().GetResult();

            var rawResponseString = httpResponseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult();
			Console.WriteLine($"\nRaw response :\n{rawResponseString}\n");

            var response = (PayooSOAPResponse) DeserializeXML(rawResponseString, typeof(PayooSOAPResponse));

			var responseData = (PayooResponseData.GetBill) DeserializeXML(response.Body.Execute2Response.Execute2Result.Data, typeof(PayooResponseData.GetBill));

			Console.WriteLine($"\nReturn code : {responseData.ReturnedCode}\n");
            Console.WriteLine($"\nResponse :\n{JsonConvert.SerializeObject(responseData, Newtonsoft.Json.Formatting.Indented)}");
        }

        private static void TestFakeResponseDeserialization()
        {
            var responseString = GenerateFakeResponse();
            var response = DeserializeXML(responseString, typeof(PayooResponseData.GetBill));
            Console.WriteLine("\nResponse :\n" + JsonConvert.SerializeObject(response, response.GetType(), Newtonsoft.Json.Formatting.Indented, null));
        }

        private static string GenerateRequestBodyString()
        {
            var requestTime = DateTime.UtcNow.AddHours(7).ToString("dd'/'MM'/'yyyy HHmmss");
            var checksum = Hash(requestTime + CLIENT_PASSWORD);
            var requestData = @"&lt;QueryBillExRequest&gt;
  &lt;UserId&gt;FTPSHOP30808&lt;/UserId&gt;
  &lt;AgentId&gt;82&lt;/AgentId&gt;
  &lt;CustomerId&gt;123132123&lt;/CustomerId&gt;
  &lt;ServiceId&gt;NUOC&lt;/ServiceId&gt;
  &lt;ProviderId&gt;CNNB&lt;/ProviderId&gt;
&lt;/QueryBillExRequest&gt;";

            var signature = Sign(requestData);

            var requestBodyString = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
  <soap:Body>
    <Execute2 xmlns=""UniGWS"">
      <request>
        <ClientId>FPTSHOP</ClientId>
        <RequestTime>{requestTime}</RequestTime>
        <Checksum>{checksum}</Checksum>
        <Operation>MMS_QueryBillEx</Operation>
        <RequestData>{requestData}</RequestData>
        <Signature>{signature}</Signature>
      </request>
    </Execute2>
  </soap:Body>
</soap:Envelope>";

            Console.WriteLine($"Request :\n{requestBodyString}");
            return requestBodyString;
        }

        private static string Hash(string rawString)
        {
            using (var md5 = MD5.Create())
            {
                var rawBytes = Encoding.UTF8.GetBytes(rawString);
                var hashBytes = md5.ComputeHash(rawBytes);

                var sb = new StringBuilder();
                foreach (var b in hashBytes)
                {
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }

        private static string Sign(string data)
        {
            using (var cert = new X509Certificate2(PKCS12_FILEPATH, PKCS12_PASSWORD))
            {
                var dataBuffer = Encoding.UTF8.GetBytes(data);
                ContentInfo contentInfo = new ContentInfo(dataBuffer);
                SignedCms signedCms = new SignedCms(contentInfo, true);
                CmsSigner cmsSigner = new CmsSigner(cert);
                cmsSigner.IncludeOption = X509IncludeOption.None;
                signedCms.ComputeSignature(cmsSigner);
                return Convert.ToBase64String(signedCms.Encode());
            }
        }

        private static string GenerateFakeResponse()
        {
            return @"<QueryBillExResult>
  <ReturnCode>0</ReturnCode>
  <Bills>
    <BillInfo>
      <BillId>1763841</BillId>
      <Month>04/2019</Month>
      <MoneyAmount>74125</MoneyAmount>
      <PaymentFee>2000</PaymentFee>
      <CustomerName>Tào Văn Á</CustomerName>
      <Address>675/28 TRAN ...</Address>
      <ServiceId>NUOC</ServiceId>
      <ProviderId>CNNB</ProviderId>
      <MonthAmount p4:nil=""true"" xmlns:p4=""http://www.w3.org/2001/XMLSchema-instance"" />
      <InfoEx>675/28 TRAN ...</InfoEx>
      <ShippingDateNum p4:nil=""true"" xmlns:p4=""http://www.w3.org/2001/XMLSchema-instance"" />
      <PaymentRange>1-1</PaymentRange>
      <IsPrepaid>false</IsPrepaid>
      <ExpiredDate />
      <RenewalDate />
      <OrderNo />
      <ShopName />
      <CustomerId>123132123</CustomerId>
      <InfoBill />
    </BillInfo>
  </Bills>
  <Services>
    <Service>
      <ServiceId>NUOC</ServiceId>
      <MatchProviderCount>0</MatchProviderCount>
      <Issuers>
        <Issuer>
          <IssuerId>CNNB</IssuerId>
          <IsOnline>true</IsOnline>
        </Issuer>
      </Issuers>
    </Service>
  </Services>
  <VietUnionId />
  <PaymentRule>2</PaymentRule>
  <MatchServiceCount>0</MatchServiceCount>
  <IsUseReceivedBill>false</IsUseReceivedBill>
  <ViewOptions />
  <PaymentFeeType>2</PaymentFeeType>
  <PercentFee>0</PercentFee>
  <ConstantFee>2000</ConstantFee>
  <MinFee>0</MinFee>
  <MaxFee>1000000</MaxFee>
</QueryBillExResult>";
        }

        private static object DeserializeXML(string xml, Type type)
        {
            var serializer = new XmlSerializer(type);

            using (var stringReader = new StringReader(xml))
            {
                return serializer.Deserialize(stringReader);
            }
        }

    }
}
