﻿using System;
using System.Xml.Serialization;

namespace test_payoo
{
    #region Request

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class PayooSOAPRequest
    {
        private XmlSerializerNamespaces xmlns;

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces Xmlns
        {
            get
            {
                if (xmlns == null)
                {
                    xmlns = new XmlSerializerNamespaces();
                    xmlns.Add("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                }

                return xmlns;
            }
            set { xmlns = value; }
        }

        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public PayooSOAPBodyRequest Body { get; set; }

        public PayooSOAPRequest() {}

        public PayooSOAPRequest(PayooRequest request)
        {
            Body = new PayooSOAPBodyRequest(request);
        }
    }

    public class PayooSOAPBodyRequest
    {
        [XmlElement(ElementName = "Execute2", Namespace = "UniGWS")]
        public PayooSOAPExecute2Request Execute2 { get; set; }

        public PayooSOAPBodyRequest() {}

        public PayooSOAPBodyRequest(PayooRequest request)
        {
            Execute2 = new PayooSOAPExecute2Request(request);
        }
    }

    public class PayooSOAPExecute2Request
    {
        private XmlSerializerNamespaces xmlns;

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces Xmlns
        {
            get
            {
                if (xmlns == null)
                {
                    xmlns = new XmlSerializerNamespaces();
                    xmlns.Add("", "UniGWS");
                }

                return xmlns;
            }
            set { xmlns = value; }
        }

        [XmlElement(ElementName = "request")]
        public PayooRequest Request { get; set; }

        public PayooSOAPExecute2Request() {}

        public PayooSOAPExecute2Request(PayooRequest request)
        {
            Request = request;
        }
    }

    public class PayooRequest
    {
        [XmlElement(ElementName = "ClientId")]
        public string ClientId { get; set; }

        [XmlElement(ElementName = "RequestTime")]
        public string Time { get; set; }
        
        [XmlElement(ElementName = "Checksum")]
        public string Checksum { get; set; }
        
        [XmlElement(ElementName = "Operation")]
        public string Operation { get; set; }
        
        [XmlElement(ElementName = "RequestData")]
        public string Data { get; set; }
        
        [XmlElement(ElementName = "Signature")]
        public string Signature { get; set; }
    }

    public abstract partial class PayooRequestData : IRequest
    {
        [XmlIgnore]
        public string RequestId { get; set; }

        [XmlElement(ElementName = "UserId")]
        public string RequesterId { get; set; }

        [XmlIgnore]
        public string WarehouseCode { get; set; }

        [XmlIgnore]
        public string IPAddress { get; set; }

        [XmlElement(ElementName = "AgentId")]
        public string AgentId { get; set; }

        public string RegionCode { get; set; }

        [XmlIgnore]
        public abstract string Operation { get; }

        [XmlIgnore]
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

        public abstract Type GetResponseType();
    }

    #endregion

    #region Response

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class PayooSOAPResponse
    {
        private XmlSerializerNamespaces xmlns;

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces Xmlns
        {
            get
            {
                if (xmlns == null)
                {
                    xmlns = new XmlSerializerNamespaces();
                    xmlns.Add("", "http://schemas.xmlsoap.org/soap/envelope/");
                }

                return xmlns;
            }
            set { xmlns = value; }
        }

        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public PayooSOAPBodyResponse Body { get; set; }
    }

    public class PayooSOAPBodyResponse
    {
        [XmlElement(ElementName = "Execute2Response", Namespace = "UniGWS")]
        public PayooSOAPExecute2Response Execute2Response { get; set; }
    }

    public class PayooSOAPExecute2Response
    {
        private XmlSerializerNamespaces xmlns;

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces Xmlns
        {
            get
            {
                if (xmlns == null)
                {
                    xmlns = new XmlSerializerNamespaces();
                    xmlns.Add("", "UniGWS");
                }

                return xmlns;
            }
            set { xmlns = value; }
        }

        [XmlElement(ElementName = "Execute2Result")]
        public PayooResponse Execute2Result { get; set; }
    }

    public class PayooResponse
    {
        [XmlElement(ElementName = "ResponseData")]
        public string Data { get; set; }
        
        [XmlElement(ElementName = "Signature")]
        public string Signature { get; set; }
    }

    public abstract partial class PayooResponseData : IResponse
    {
        [XmlIgnore]
        public string RequestId { get; set; }

        [XmlIgnore]
        public string RequesterId { get; set; }
        
        [XmlElement(ElementName = "ReturnCode")]
        public string ReturnedCode { get; set; }

        [XmlIgnore]
        public string ReturnedMessage { get; set; }

        [XmlIgnore]
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    }

    #endregion

}
