namespace test_payoo
{
    public interface IResponse : IMessage
    {
        string RequestId { get; set; }
        string RequesterId { get; set; }
        string ReturnedCode { get; set; }
        string ReturnedMessage { get; set; }
    }
}